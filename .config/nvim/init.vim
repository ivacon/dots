set relativenumber
set noshowmode

call plug#begin()
Plug 'itchyny/lightline.vim'
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'preservim/nerdtree'
Plug 'folke/tokyonight.nvim'
Plug 'romgrk/barbar.nvim'
Plug 'kyazdani42/nvim-web-devicons'
call plug#end()

colorscheme tokyonight
let g:lightline = { 'colorscheme': 'tokyonight' }

set guifont=MesloLGS\ NF:h10

set shiftwidth=4
set tabstop=4
set smarttab
set expandtab


