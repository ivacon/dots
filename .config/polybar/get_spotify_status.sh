PLAYER="spotify"
FORMAT="{{ title }} - {{ artist }}"

playerctl --player=$PLAYER metadata --format "$FORMAT"
