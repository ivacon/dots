# My dotfiles

My dotfiles. Not too much.
Colour scheme: Tokyo Night

## Programs
- Alacritty (terminal)
- Neovim/Neovide (editor)
- Nuclear (music player)
- Btop/Gtop (system monitor)
- Polybar (bar)
- i3-gaps (WM)

